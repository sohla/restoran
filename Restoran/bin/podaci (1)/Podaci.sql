--
-- File generated with SQLiteStudio v3.1.1 on чет мај 17 20:53:44 2018
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Jela
CREATE TABLE Jela (nazivRestorana REFERENCES Restorani (nazivRestorana) NOT NULL, nazivJela NOT NULL, cena DOUBLE NOT NULL, opis STRING NOT NULL, kolicina INTEGER NOT NULL);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Gregor''s pub', 'paradajiz corba', 160, 'paradajiz corba', 150);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Gregor''s pub', 'gulas', 320, 'svinjetina, sargarepa, luk, paradajiz', 350);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Gregor''s pub', 'omlet', 170, '3 komada, pavlaka, paradajiz', 250);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Gregor''s pub', 'gregors'' meze', 360, 'sir, feta, julen, pecenica, masline, pecurke, pohovani stapici', 250);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Roma', 'roma pica', 550, 'punjeni rub, origano, sunka, sampinjoni, rolnice, suvi vrat, jaje', 590);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Roma', 'vezuvio', 500, 'pelat, origano, sunka, sir, masline', 460);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Roma', 'quattro formaggio', 540, 'pelat, orignao, sir, feta, masline, bosilja, sampinjoni', 480);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Roma', 'margarita', 480, 'pelat, origano, sir, masline', 420);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Greda', 'sopska', 260, 'sopska salata', 240);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Greda', 'cufte', 399, 'cufte u sosu', 420);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Greda', 'porcija cevapa', 329, '7 komada, domaci hleb', 330);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Greda', 'bela vesalica', 415, 'bela vesalica, domaci hleb', 200);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Index house', 'palacinka pica', 180, 'sunka, sir, sampinjoni, pavlaka, mirinirani sampinjoni', 200);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Index house', 'index', 200, 'praska sunka, sir , saminjoni', 230);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Index house', 'dupli burger', 210, 'meso, topljeni sir, majonez, ajsberg, krastavac', 190);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Index house', 'burger', 140, 'meso, topljeni sir, majonez, ajsberg, krastavac', 120);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Index house', 'sendvic pileci file', 160, 'pileca prsa, pavlaka, majonez, zelena', 150);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Hedone', 'cezar salata', 360, 'zelana, pileci file, panceta, krutoni, dresing, ceri, pamezan', 400);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Hedone', 'karbonara', 390, 'panceta, pavlaka, parmezan, luk, jaje', 450);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Hedone', 'ramstek na zaru', 645, 'ramstek,sararepa, krompir', 250);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Hedone', 'rolovana piletin', 495, 'piletina,slanina,sampinjoni,sir,pomfrit', 300);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Giros land', 'pljeskavic', 220, 'meso', 170);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Giros land', 'Dupli', 370, 'piletina pomfrit', 350);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Giros land', 'giros vege', 180, 'tortilja, salta, pomfit', 200);
INSERT INTO Jela (nazivRestorana, nazivJela, cena, opis, kolicina) VALUES ('Giros land', 'pileci giros', 280, 'meso, pomfrit', 200);

-- Table: Korisnici
CREATE TABLE Korisnici (tip STRING NOT NULL, ime STRING NOT NULL, prezime STRING NOT NULL, pol STRING NOT NULL, koriscnicko STRING NOT NULL PRIMARY KEY, lozinka STRING NOT NULL, jmbg NUMERIC NOT NULL, plata DOUBLE NOT NULL, tipVozila STRING NOT NULL);

-- Table: Restorani
CREATE TABLE Restorani (nazivRestorana STRING PRIMARY KEY NOT NULL, adresa STRING NOT NULL, kategorija NOT NULL);
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Gregor''s pub', 'Hajduk veljkova 11', 'Srpska hrana');
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Roma', 'Pap Pavla 22', 'Italijanska hrana');
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Giros land', 'Laze Teleckog 23', 'Grcka hrana');
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Greda', 'Jevrejska 26', 'Srpska hrana');
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Index house', 'Bulevar mihajla Pupina 5', 'Brza hrana');
INSERT INTO Restorani (nazivRestorana, adresa, kategorija) VALUES ('Hedone', 'Kornelija Stankovic 13', 'italijanska hrana');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
