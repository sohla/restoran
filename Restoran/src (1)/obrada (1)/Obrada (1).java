package obrada;
import java.util.ArrayList;

import osobe.*;
import restoran.*;

public class Obrada {
	private ArrayList<Restoran> restorani;
	private ArrayList<Artikal> artikli;
	private ArrayList<Porudzbina> porudzbine;
	private ArrayList<Kupac> kupci;
	private ArrayList<Dostavljac> dostavljaci;
	private ArrayList<Administrator> administatori;

	public Obrada() {
		this.restorani = new ArrayList<Restoran>();
		this.artikli = new ArrayList<Artikal>();
		this.porudzbine = new ArrayList<Porudzbina>();
		this.kupci = new ArrayList<Kupac>();
		this.dostavljaci = new ArrayList<Dostavljac>();
		this.administatori = new ArrayList<Administrator>();

	}
	
	public ArrayList<Restoran> getRestoran(){
		return restorani;
		
	}
	
	public void dodajRestoran(Restoran restoran){
		this.restorani.add(restoran);
	}
	
	public void obrisiRestoran(Restoran restoran){
		this.restorani.remove(restoran);
	}
	
	public ArrayList<Artikal> getArtikal(){
		return artikli;
	}
	
	public void dodajArtikal(Artikal artikal) {
		this.artikli.add(artikal);
	}
	
	public void obrisiArtikal(Artikal artikal) {
		this.artikli.remove(artikal);
	}
	
	public ArrayList<Porudzbina> getPorudzbina(){
		return porudzbine;
	}
	
	public void dodajPorudzbinu(Porudzbina porudzbina){
		this.porudzbine.add(porudzbina);
	}
	public void obrisiPorudzbinu(Porudzbina porudzbina){
		this.porudzbine.remove(porudzbina);
	}
	
	public ArrayList<Kupac> getKupac(){
		return kupci;
	}
	
	public void dodajKupca(Kupac kupac){
		this.kupci.add(kupac);
	}
	public void obrisiKupca(Kupac kupac){
		this.kupci.remove(kupac);
	}
	
	public ArrayList<Dostavljac> getDostavljac(){
		return dostavljaci;
	}
	
	public void dodajDostavljaca(Dostavljac dostavljac){
		this.dostavljaci.add(dostavljac);
	}
	
	public void obrisiDostavljaca(Dostavljac dostavljac){
		this.dostavljaci.remove(dostavljac);
	}
	
	public ArrayList<Administrator> getAdministrator(){
		return administatori;
	}
	public void dodajAdministatora(Administrator administator){
		this.administatori.add(administator);
	}
	public void obrisiAdministratora(Administrator administrator){
		this.administatori.remove(administrator);
	}
	

	
}



