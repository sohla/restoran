package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;

import net.proteanit.sql.DbUtils;

public class Brisanje  {

	
	
	JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public void Brisanje() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Brisanje window= new Brisanje();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}



	/**
	 * @wbp.parser.entryPoint
	 */
	
	Connection connection = null;
	
	public Brisanje() {
		
		connection = SQLite.dbConnector();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnRestorani = new JButton("Restorani");
		btnRestorani.setBounds(92, 22, 105, 27);
		frame.getContentPane().add(btnRestorani);
		
		JButton btnHrana = new JButton("Hrana");
		btnHrana.setBounds(224, 22, 105, 27);
		frame.getContentPane().add(btnHrana);
		
		JButton btnOsobe = new JButton("Osobe");
		btnOsobe.setBounds(363, 22, 105, 27);
		frame.getContentPane().add(btnOsobe);

	   	table = new JTable();		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(92, 88, 595, 196);
		frame.getContentPane().add(scrollPane);
		
		JButton btnObrisi = new JButton("Obrisi");
		btnObrisi.setBounds(543, 22, 144, 27);
		frame.getContentPane().add(btnObrisi);

 
    	btnRestorani.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from Restorani";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
					
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}
			
		});
    	
    	btnHrana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from jela";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
					
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}
			
		});
    	
    	btnOsobe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from korisnici";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
					
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}
			
		});
		
	}
}
