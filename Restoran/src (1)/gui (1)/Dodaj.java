package gui;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import DAO.UserDAO;

import java.awt.event.ActionEvent;

public class Dodaj {
	
	

	private JPanel contentPane;
	JFrame frame;

	/**
	 * Launch the application.
	 */
	public void OvoOno() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dodaj window = new Dodaj();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection connection = null;
	public static JTextField rstnIme;
	public static JTextField rstnAdresa;
	public static JTextField krsnIme;
	public static JTextField krsnPrezime;
	public static JTextField krsnKorisnicko;
	public static JTextField krsnLozinka;
	public static JTextField krsnJMBG;
	public static JTextField krsnPlata;
	public static JTextField jeloRestoran;
	public static JTextField jeloNaziv;
	public static JTextField jeloCena;
	public static JTextField jeloOpis;
	public static JTextField jeloKolicina;
	public static JComboBox krsnPol;
	public static JComboBox<String> krsnTipVozila;
	public static JButton btnDodajKorisnika;
	//public static String krsnKorisnik;
	
	public Dodaj() {
		connection = SQLite.dbConnector();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDodajRestoran = new JLabel("dodaj restoran");
		lblDodajRestoran.setBounds(342, 12, 96, 17);
		frame.getContentPane().add(lblDodajRestoran);
		
		JLabel lblIme = new JLabel("ime");
		lblIme.setBounds(71, 29, 60, 17);
		frame.getContentPane().add(lblIme);
		
		JLabel lblAdresa = new JLabel("adresa");
		lblAdresa.setBounds(236, 29, 60, 17);
		frame.getContentPane().add(lblAdresa);
		
		JLabel lblKategorija = new JLabel("kategorija");
		lblKategorija.setBounds(362, 29, 76, 17);
		frame.getContentPane().add(lblKategorija);
		
		rstnIme = new JTextField();
		rstnIme.setBounds(35, 58, 114, 21);
		frame.getContentPane().add(rstnIme);
		rstnIme.setColumns(10);
		
		rstnAdresa = new JTextField();
		rstnAdresa.setBounds(196, 58, 114, 21);
		frame.getContentPane().add(rstnAdresa);
		rstnAdresa.setColumns(10);
		
		String[] tipoviRestorana  = {"Grcha hrana","Srpska hrana","Italijanska hrana","Brza hrana"};
		
		JComboBox rstnTip = new JComboBox(tipoviRestorana);
		rstnTip.setBounds(342, 55, 84, 24);
		frame.getContentPane().add(rstnTip);
		
		
		JLabel lblKorisnici = new JLabel("korisnici");
		lblKorisnici.setBounds(342, 89, 60, 17);
		frame.getContentPane().add(lblKorisnici);
		
		JLabel lblNewLabel = new JLabel("tip");
		lblNewLabel.setBounds(71, 110, 60, 17);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblIme_1 = new JLabel("ime");
		lblIme_1.setBounds(187, 110, 60, 17);
		frame.getContentPane().add(lblIme_1);
		
		JLabel lblPrezime = new JLabel("prezime");
		lblPrezime.setBounds(292, 110, 60, 17);
		frame.getContentPane().add(lblPrezime);
		
		JLabel lblPol = new JLabel("pol");
		lblPol.setBounds(413, 110, 60, 17);
		frame.getContentPane().add(lblPol);
		
		JLabel lblKorisnicko = new JLabel("korisnicko");
		lblKorisnicko.setBounds(511, 110, 84, 17);
		frame.getContentPane().add(lblKorisnicko);
		
		JLabel lblLozinka = new JLabel("lozinka");
		lblLozinka.setBounds(652, 110, 60, 17);
		frame.getContentPane().add(lblLozinka);
		
		JLabel lblJbmg = new JLabel("jbmg");
		lblJbmg.setBounds(71, 190, 60, 17);
		frame.getContentPane().add(lblJbmg);
		
		String[] tipKorisnika= {"administator","kupac","dostavljac"};
		JComboBox krsnKorisnik = new JComboBox(tipKorisnika);
		krsnKorisnik.setBounds(35, 139, 96, 21);
		frame.getContentPane().add(krsnKorisnik);
		
		krsnIme = new JTextField();
		krsnIme.setBounds(143, 139, 114, 21);
		frame.getContentPane().add(krsnIme);
		krsnIme.setColumns(10);
		
		krsnPrezime = new JTextField();
		krsnPrezime.setBounds(269, 139, 114, 21);
		frame.getContentPane().add(krsnPrezime);
		krsnPrezime.setColumns(10);
		
		String[] polKorisnika= {"muski","zenski"};
		JComboBox krsnPol = new JComboBox(polKorisnika);
		krsnPol.setBounds(395, 139, 88, 21);
		frame.getContentPane().add(krsnPol);
		
		krsnKorisnicko = new JTextField();
		krsnKorisnicko.setBounds(508, 139, 114, 21);
		frame.getContentPane().add(krsnKorisnicko);
		krsnKorisnicko.setColumns(10);
		
		krsnLozinka = new JTextField();
		krsnLozinka.setBounds(629, 139, 114, 21);
		frame.getContentPane().add(krsnLozinka);
		krsnLozinka.setColumns(10);
		
		krsnJMBG = new JTextField();
		krsnJMBG.setBounds(30, 227, 114, 21);
		frame.getContentPane().add(krsnJMBG);
		krsnJMBG.setColumns(10);
		
		JLabel lblPlata = new JLabel("plata");
		lblPlata.setBounds(187, 190, 60, 17);
		frame.getContentPane().add(lblPlata);
		
		String[] tipVozila = {"Bicikl","Skuter","Automobil","Kombi"};
		JComboBox krsnTipVozila = new JComboBox(tipVozila);
		krsnTipVozila.setBounds(275, 224, 108, 26);
		frame.getContentPane().add(krsnTipVozila);
		
		krsnPlata = new JTextField();
		krsnPlata.setBounds(156, 227, 114, 21);
		frame.getContentPane().add(krsnPlata);
		krsnPlata.setColumns(10);
		
		JLabel lblTipVozila = new JLabel("tip vozila");
		lblTipVozila.setBounds(292, 190, 60, 17);
		frame.getContentPane().add(lblTipVozila);
		
		JLabel lblJela = new JLabel("Jela");
		lblJela.setBounds(356, 262, 60, 17);
		frame.getContentPane().add(lblJela);
		
		JLabel lblNaviz = new JLabel("naviz");
		lblNaviz.setBounds(48, 297, 60, 17);
		frame.getContentPane().add(lblNaviz);
		
		JLabel lblJelo = new JLabel("jelo");
		lblJelo.setBounds(217, 297, 60, 17);
		frame.getContentPane().add(lblJelo);
		
		JLabel lblCena = new JLabel("cena");
		lblCena.setBounds(366, 297, 60, 17);
		frame.getContentPane().add(lblCena);
		
		JLabel lblOpis = new JLabel("opis");
		lblOpis.setBounds(508, 297, 60, 17);
		frame.getContentPane().add(lblOpis);
		
		JLabel lblKolicina = new JLabel("kolicina");
		lblKolicina.setBounds(629, 297, 60, 17);
		frame.getContentPane().add(lblKolicina);
		
		jeloRestoran = new JTextField();
		jeloRestoran.setBounds(30, 326, 114, 21);
		frame.getContentPane().add(jeloRestoran);
		jeloRestoran.setColumns(10);
		
		jeloNaziv = new JTextField();
		jeloNaziv.setBounds(182, 326, 114, 21);
		frame.getContentPane().add(jeloNaziv);
		jeloNaziv.setColumns(10);
		
		jeloCena = new JTextField();
		jeloCena.setBounds(316, 326, 114, 21);
		frame.getContentPane().add(jeloCena);
		jeloCena.setColumns(10);
		
		jeloOpis = new JTextField();
		jeloOpis.setBounds(454, 326, 114, 21);
		frame.getContentPane().add(jeloOpis);
		jeloOpis.setColumns(10);
		
		jeloKolicina = new JTextField();
		jeloKolicina.setBounds(580, 326, 114, 21);
		frame.getContentPane().add(jeloKolicina);
		jeloKolicina.setColumns(10);
				
		JButton btnDodajRestoran = new JButton("Dodaj restoran");
		btnDodajRestoran.setBounds(463, 55, 147, 27);
		frame.getContentPane().add(btnDodajRestoran);
		
		JButton btnDodajKorisnika = new JButton("Dodaj korisnika");
		btnDodajKorisnika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					System.out.println("44");
					UserDAO ud = new UserDAO();
					
				/*	String query ="insert into korisnici values (?,?,?,?,?,?)";
					PreparedStatement ps= connection.prepareStatement(query);
					ps.setString(1, (String) Dodaj.krsnTip.getSelectedItem());
					ps.setString(2, Dodaj.krsnIme.getText());
					ps.setString(3, Dodaj.krsnPrezime.getText());
					ps.setString(4, (String) Dodaj.krsnPol.getSelectedItem());
					ps.setString(5, Dodaj.krsnKorisnicko.getText());
					ps.setString(6, Dodaj.krsnLozinka.getText());
					ps.setString(7, Dodaj.krsnJMBG.getText());
					ps.setString(8, Dodaj.krsnPlata.getText());
					ps.setString(9, (String) Dodaj.krsnTipVozila.getSelectedItem());
					UserDAO ud = new UserDAO();
		*/		} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDodajKorisnika.setBounds(413, 224, 138, 27);
		frame.getContentPane().add(btnDodajKorisnika);
		
		JButton btnDodajJelo = new JButton("Dodaj jelo");
		btnDodajJelo.setBounds(431, 359, 105, 27);
		frame.getContentPane().add(btnDodajJelo);
	
		
	}
	}

