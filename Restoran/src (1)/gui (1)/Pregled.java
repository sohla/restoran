package gui;


import restoran.Restoran;
import net.proteanit.sql.DbUtils;

import javax.swing.*;

import java.sql.*;
import java.awt.EventQueue;

import javax.swing.AbstractButton;
import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.TableModel;

import net.proteanit.sql.DbUtils;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Choice;

public class Pregled {

	JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 * @return 
	 */
	public void Svea() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pregled window = new Pregled();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pregled() {
		Start();
	}
	Connection connection = null;
	/**
	 * Initialize the contents of the frame.
	 */
	private void Start() {
		connection = SQLite.dbConnector();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		

		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(81, 167, 628, 221);
		frame.getContentPane().add(scrollPane1); 
		
	/*	table = new JTable();
		table.addMouseListener(new MouseAdapter() {   ///ocitava red iz tabela koja je u jscrolpane
			@Override 
			public void mouseClicked(MouseEvent e) {
			}
		}); */
		
		table = new JTable();
  	
		scrollPane1.setViewportView(table);
		JButton btnUcitaj = new JButton("Ocitaj restorane");
		btnUcitaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from Restorani";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					if(rs.next()){
						Restoran restoran = new Restoran();
						restoran.setNaziv(rs.getString("naziv"));
						restoran.setKategorija(rs.getString("kategorija"));
						System.out.println("asdaad");
						
						//return restoran;
						
						
					}
					
		

					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}
			
		});
		btnUcitaj.setBounds(149, 9, 95, 27);
		frame.getContentPane().add(btnUcitaj);
		
		
		
		
		
		JButton btnJela = new JButton("Ocitaj hranu");
		btnJela.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from Jela"; 
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
				
					DatabaseMetaData md = connection.getMetaData();
					ResultSet rss = md.getAttributes(null, null, "%", null);
					while (rss.next()){
						System.out.printf("table name: %s%n", rss.getString("TABLE_NAME"));
					}
					
					table.addMouseListener(new MouseAdapter() {   ///ocitava red iz tabela koja je u jscrolpane
						public void mouseClicked(MouseEvent e) {
							System.out.print("klik lik");
							
							try {
								DatabaseMetaData md = connection.getMetaData();
								ResultSet rss = md.getAttributes(null, null, "%", null);
								while (rss.next()){
									System.out.printf("table name: %s%n", rss.getString("TABLE_NAME"));
								}
								//napravi da ovo radi samo kad su jela octina
							}catch (Exception exc) {
								
								
								System.out.print("de klikces druze?!");
							}
						/*	int i = table.getSelectedRow();
							TableModel mdl = table.getModel();
							String a = mdl.getValueAt(i, 0).toString();
						   //puca program jer hrana ima 5 kolona, restorani ne!!
							String b = mdl.getValueAt(i, 1).toString();
							String c = mdl.getValueAt(i, 2).toString();
							String d = mdl.getValueAt(i, 3).toString();
							String x = mdl.getValueAt(i, 4).toString();
							System.out.println("a"); 
							System.out.println(a); */
						    //JPanel rstn.setText() = a;//dodaj da upisuje u polja: rstn, op, cn nz ..., nakon toga dugme Dodaj, pa korpa pa porudzbina
														//upisuje porudzbinu sa adreson i usernamnom u sql porudzbina + tacno vreme
				
						}
					
					});
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					

					
				} catch (Exception ex) {
					ex.printStackTrace();
					System.out.println("asd");
				}
				
				
			}
			
			
				
		});
		
	
		
	    
		
		btnJela.setBounds(149, 48, 95, 27);
		frame.getContentPane().add(btnJela);
		
		JButton btnDodaj = new JButton("Dodaj");
		btnDodaj.setBounds(506, 12, 114, 74);
		frame.getContentPane().add(btnDodaj);
		
		String [] vrstaRestorana = {"Srpska hrana", "Brza hrana", "Italijanska hrana", "Grcka hrana"};
		
		JComboBox Vrsta = new JComboBox(vrstaRestorana);
		Vrsta.setBounds(149, 87, 95, 27);
		frame.getContentPane().add(Vrsta);
		
		JTextPane rstn = new JTextPane();
		rstn.setBounds(340, 12, 132, 23);
		frame.getContentPane().add(rstn);
		//rstn.setText(a1);
		//rstn.setText("a");    //dodavanje
		
		JTextPane nzv = new JTextPane();
		nzv.setBounds(340, 43, 132, 23);
		frame.getContentPane().add(nzv);
		String a = null;
		nzv.setText(a);
		
		JTextPane cn = new JTextPane();
		cn.setBounds(340, 73, 132, 23);
		frame.getContentPane().add(cn);
		
		JTextPane ops = new JTextPane();
		ops.setBounds(340, 103, 132, 23);
		frame.getContentPane().add(ops);
		
		JTextPane klc = new JTextPane();
		klc.setBounds(340, 132, 132, 23);
		frame.getContentPane().add(klc);
		
		JLabel lblRestoran = new JLabel("restoran");
		lblRestoran.setBounds(262, 17, 60, 17);
		frame.getContentPane().add(lblRestoran);
		
		JLabel lblJelo = new JLabel("jelo");
		lblJelo.setBounds(262, 48, 60, 17);
		frame.getContentPane().add(lblJelo);
		
		JLabel lblCena = new JLabel("cena");
		lblCena.setBounds(262, 79, 60, 17);
		frame.getContentPane().add(lblCena);
		
		JLabel lblOpis = new JLabel("opis");
		lblOpis.setBounds(262, 108, 60, 17);
		frame.getContentPane().add(lblOpis);
		
		JLabel lblKolicina = new JLabel("kolicina");
		lblKolicina.setBounds(262, 138, 60, 17);
		frame.getContentPane().add(lblKolicina);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(506, 129, 114, 26);
		frame.getContentPane().add(comboBox);
		

		
		
		
	//	rdbtnRestoran.addActionListener(this);
	//	rdbtnHrana.addActionListener(this);
		
		Vrsta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
				String izabrano = (String)cb.getSelectedItem();
					try {
					String query = "select * from Restorani where kategorija='"+izabrano+"'";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception ex) {
					ex.printStackTrace();
				} 
				
				
			}
			
				
			});
		
		
	}
}
