package gui;

import java.awt.EventQueue;
import java.awt.Window;
import java.sql.*;

import javax.swing.*;

import main.Izbor;
import gui.Pregled;

//import com.sun.javafx.tk.Toolkit;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JPanel;

import java.awt.Color;

import javax.swing.JTextArea;
import javax.swing.JTextPane;


public class Login {

	private JFrame frame;
	private JTextField username;
	private JTextField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	Connection connection = null;
	private JLabel lblTipKorisnika;
	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		connection = SQLite.dbConnector();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 300, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("username");
		lblUsername.setBounds(120, 29, 83, 27);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(120, 68, 83, 35);
		frame.getContentPane().add(lblPassword);
		
		username = new JTextField();
		username.setBounds(221, 32, 114, 21);
		frame.getContentPane().add(username);
		username.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(221, 75, 114, 21);
		frame.getContentPane().add(passwordField);
		passwordField.setColumns(10);
		

		JButton btnLogin = new JButton("Login");
		frame.getContentPane().add(btnLogin);	
		btnLogin.setBounds(221, 172, 114, 27);
		
		lblTipKorisnika = new JLabel("tip korisnika");
		lblTipKorisnika.setBounds(120, 115, 83, 17);
		frame.getContentPane().add(lblTipKorisnika);
		
		String[] tipoviKorisnika  = {"","kupac","dostavljac","administrator"};
		
		//JComboBox jComboBox = new JComboBox(tipoviKorisnika);
		final JComboBox cmbTipKorisnika =new JComboBox(tipoviKorisnika);
		cmbTipKorisnika.setBounds(221, 118, 114, 21);
		frame.getContentPane().add(cmbTipKorisnika);
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Object tip = "";
					String query = "select * from Korisnici where korisnicko=? and lozinka=? and tip=?";
					PreparedStatement pst= connection.prepareStatement(query);
					pst.setString(1,username.getText());
					pst.setString(2,passwordField.getText());
					pst.setObject(3,cmbTipKorisnika.getSelectedItem()); 
					ResultSet rs = pst.executeQuery();
					
					tip = cmbTipKorisnika.getSelectedItem();
					
					//System.out.print(tip);
					
					int count = 0;
					
					while(rs.next()){
						count +=1;
					}	
					if (count == 1 ) {
						JOptionPane.showMessageDialog(null,"Uspesan login");
						if(tip=="kupac"){
							Pregled window = new Pregled();
							window.frame.setVisible(true);
							System.out.println("tip");
						}
						
						else if(tip=="dostavljac"){
							System.out.println("dostavljac");
							Dostavljac window = new Dostavljac();
							window.frame.setVisible(true);

							
							
						}else if(tip=="administrator"){
							System.out.println("administrator");
							AdminIzbor window = new AdminIzbor();
							window.frame.setVisible(true);
							//Login.setVisible(false);
		
		
							//meni admina
							
						}
				//	}else if(count>1) {
					///	JOptionPane.showMessageDialog(null,"Nespesan login");
					}
					else{
						JOptionPane.showMessageDialog(null,"Nespesan login, pokusajte ponovo");
					}
					rs.close();
					pst.close();
				}catch(Exception eee){
						JOptionPane.showMessageDialog(null, eee);
					}
			
				}	
			});	
		
		}

	protected void Sve() {
		// TODO Auto-generated method stub
		
	}
}
	