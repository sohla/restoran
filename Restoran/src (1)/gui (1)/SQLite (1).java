package gui;
import java.sql.*;
import javax.swing.*;

public class SQLite {
	Connection conn = null;
	public static Connection dbConnector() {

		try {
			
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:src/podaci/Podaci");
			System.out.println("bd povezana");
			return conn;
			
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
			System.out.println("bd NIJE povezana");
			return null;
		}
		
	}
}
