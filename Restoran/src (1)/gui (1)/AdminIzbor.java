package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;

public class AdminIzbor {

	private JPanel table;
	JFrame frame;

	/**
	 * Launch the application.
	 */
	public void AdminIzbor() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminIzbor window = new AdminIzbor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	public AdminIzbor() {
		frame = new JFrame();
		frame.setBounds(200, 200, 800, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnDodavanje = new JButton("Dodavanje");
		btnDodavanje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Dodaj dodaj = new Dodaj();
				dodaj.frame.setVisible(true);
			}
		});
		btnDodavanje.setBounds(272, 37, 247, 27);
		frame.getContentPane().add(btnDodavanje);
		
		JButton btnBrisanje = new JButton("Brisanje");
		btnBrisanje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Brisanje brisanje = new Brisanje();
				brisanje.frame.setVisible(true);
			}
		});
		btnBrisanje.setBounds(272, 100, 247, 27);
		frame.getContentPane().add(btnBrisanje);
		
		JButton btnIzmena = new JButton("Izmena");
		btnIzmena.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnIzmena.setBounds(272, 152, 247, 27);
		frame.getContentPane().add(btnIzmena);
	}

}
