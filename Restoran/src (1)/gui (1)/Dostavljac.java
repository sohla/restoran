package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import net.proteanit.sql.DbUtils;
import net.proteanit.sql.DbUtils;
public class Dostavljac{

	private JTable table;
	JFrame frame;

	/**
	 * Launch the application.
	 */
	public void Dostavljac() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dostavljac window = new Dostavljac();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection connection = null;
	
	public Dostavljac() {
		connection = SQLite.dbConnector();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(81, 110, 628, 278);
		frame.getContentPane().add(scrollPane1);
		
		
		JButton btnDostava = new JButton("Ocitaj porudzine");
		btnDostava.setBounds(329, 58, 210, 27);
		frame.getContentPane().add(btnDostava);	
		
		table = new JTable();
		scrollPane1.setViewportView(table);
		btnDostava.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "select * from Porudzbina";
					PreparedStatement pst = connection.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}
			
		});
	}
}
