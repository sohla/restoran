package osobe;

public class Kupac extends Osoba {
	private String adresa;
	private String brTelefona;

	public Kupac() {
		this.adresa = "";
		this.brTelefona = "";		
	}
	
	public Kupac(String ime, String prezime, String pol,String korisnicko,String lozinka,String adresa,String brTelefon, String brTelefona){
		this.adresa = adresa;
		this.brTelefona = brTelefona;
	}
	public Kupac(Kupac original){
		super(original);
		this.adresa = original.adresa;
		this.brTelefona = original.brTelefona;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(String brTelefona) {
		this.brTelefona = brTelefona;
	}

	@Override
	public String toString() {
		return "\nadresa=" + adresa + 
				"\nbrTelefona=" + brTelefona ;
	}
	
}


