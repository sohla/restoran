package osobe;

public class Dostavljac extends Osoba {
	private String registracija;
	private String tipVozila;
	
	public Dostavljac() {
		this.registracija = "";
		this.tipVozila = "";
	}
	public Dostavljac(String ime, String prezime,String pol,String korisnicko,String lozinka, String registracija, String tipVozila) {
		this.registracija = registracija;
		this.tipVozila = tipVozila;
	}
	public Dostavljac(Dostavljac original){

		this.registracija = original.registracija;
		this.tipVozila = original.tipVozila;
	}

	public String getRegistracija() {
		return registracija;
	}
	public void setRegistracija(String registracija) {
		this.registracija = registracija;
	}
	public String getTipVozila() {
		return tipVozila;
	}
	public void setTipVozila(String tipVozila) {
		this.tipVozila = tipVozila;
	}
	@Override
	public String toString() {
		return 
				"\nregistracija=" + registracija + 
				"\ntipVozila=" + tipVozila;
	}

}

	
