package osobe;

public class Administrator extends Osoba {
	private int JMBG;	
	private double plata;
	
	public Administrator() {
		this.JMBG = 0;
		this.plata = 0;
	}
	public Administrator(String ime, String prezime, String pol,String korisnicko,String lozinka,int JMBG, double plata) {
		this.JMBG = JMBG;	
		this.plata = plata;
	}
	public Administrator(Administrator original) {
		super(original);
		this.JMBG = original.JMBG;
		this.plata = original.plata;
	}
	public int getJMBG() {
		return JMBG;
	}
	public void setJMBG(int jMBG) {
		JMBG = jMBG;
	}
	public double getPlata() {
		return plata;
	}
	public void setPlata(double plata) {
		this.plata = plata;
	}
	@Override
	public String toString() {
		return "\nJMBG=" + JMBG +
				"\nplata=" + plata ;
	}
	
}
