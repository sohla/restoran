package osobe;

public abstract class Osoba {
		private String tip;
		private String ime;
		private String prezime;
		private String pol;
		private String korisnicko;
		private String lozinka;

		public Osoba() {
			this.tip = "";
			this.ime = "";
			this.prezime = "";
			this.pol = "";
			this.korisnicko = "";
			this.lozinka = "";
			}
		
		public Osoba(String tip, String ime, String prezime, String pol, String korisnicko, String lozinka) {
			this.tip = tip;
			this.ime = ime;
			this.prezime = prezime;
			this.pol = pol;
			this.korisnicko = korisnicko;
			this.lozinka = lozinka;
		}
		public Osoba(Osoba original) {
			this.tip = original.tip;
			this.ime = original.ime;
			this.prezime = original.prezime;
			this.pol = original.pol;
			this.korisnicko = original.korisnicko;
			this.lozinka = original.lozinka;
		}

		public String getTip() {
			return tip;
		}

		public void setTip(String tip) {
			this.tip = tip;
		}

		public String getIme() {
			return ime;
		}

		public void setIme(String ime) {
			this.ime = ime;
		}

		public String getPrezime() {
			return prezime;
		}

		public void setPrezime(String prezime) {
			this.prezime = prezime;
		}

		public String getPol() {
			return pol;
		}

		public void setPol(String pol) {
			this.pol = pol;
		}

		public String getKorisnicko() {
			return korisnicko;
		}

		public void setKorisnicko(String korisnicko) {
			this.korisnicko = korisnicko;
		}

		public String getLozinka() {
			return lozinka;
		}

		public void setLozinka(String lozinka) {
			this.lozinka = lozinka;
		}

		@Override
		public String toString() {
			return "Osoba [tip=" + tip + ", ime=" + ime + ", prezime="
					+ prezime + ", pol=" + pol + ", korisnicko=" + korisnicko
					+ ", lozinka=" + lozinka + "]";
		}
		
	}

