package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import restoran.Artikal;

public class Restoran {
	private ArrayList<Artikal> artikli;
	public void ucitajArtikle() {
		try {
			File artikliFile = new File("src/podaci/jela");
			BufferedReader br = new BufferedReader(new FileReader(artikliFile));
			String line = null;
			while((line = br.readLine()) != null){
				String[] split = line.split("\\|");
				String rest = split[0];
				String naziv = split[1];
				String cenaString = split[2];
				double cena = Double.parseDouble(cenaString);
				String opis = split[3];
				String kolicinaString = split[4];
				int kolicina = Integer.parseInt(kolicinaString);
				Artikal artikal = new Artikal(rest,naziv,cena,opis,kolicina);
				artikli.add(artikal);
			}
			br.close();
		}catch(IOException e) {
			System.out.println("nema");
		}
	}
	
	public ArrayList<Artikal> getArtikal() {
		return artikli;
	}
	
	public void dodajArtikal(Artikal artikal) {
		this.artikli.add(artikal);
	}
	
	public void obrisiArtikal(Artikal artikal) {
		this.artikli.remove(artikal);	
	}
}

