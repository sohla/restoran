package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			
		System.out.println("Unesite korisnicko ime: ");
		String username = scanner.nextLine();
			
		System.out.println("Unesite sifru: ");
		String password = scanner.nextLine();
			
		scanner.close();
			
		if(log(username, password)) {
			System.out.println("Login OK.");
		}else {
			System.out.println("Pogresni login podaci, pokusajte ponovo.");
		}
			
	}
		
	public static boolean log(String username,String password) {
		try {
			File file = new File("src/podaci/korisnici");
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null) {
				String[] lineSplit = line.split("\\|");
				String type = lineSplit[0];
				String user = lineSplit[4];
				String pass = lineSplit[5];
				if(user.equalsIgnoreCase(username) && pass.equalsIgnoreCase(password)) {					
					
					if(type.equalsIgnoreCase("administrator")) {
						System.out.println("administrator");
					return true;	
					}
					else if(type.equalsIgnoreCase("dostavljac")) {
						System.out.println("dostavljac");
					return true;	
					}
					else if(type.equalsIgnoreCase("kupac")) {
						System.out.println("kupac");
						Izbor.Skrt();
					return true;	
					}
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}


	}


