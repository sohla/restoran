package restoran;

public class Artikal extends Restoran {
	private String restoran;
	private String naziv;
	private double cena;
	private String opis;
	private double kolicina;

	public Artikal() {
		this.restoran = "";
		this.naziv = "";
		this.cena = 0;
		this.opis = "";
		this.kolicina = 0;
	}
	public Artikal(String restoran, String naziv, double cena, String opis, double kolicina) {
		this.restoran =restoran;
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
		this.kolicina = kolicina;
	}
	public Artikal(Artikal original) {
		this.restoran = original.restoran;
		this.naziv = original.naziv;
		this.cena = original.cena;
		this.opis = original.opis;
		this.kolicina = original.kolicina;
	}
	public String getRestoran() {
		return restoran;
	}
	public void setRestoran(String restoran) {
		this.restoran = restoran;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public double getKolicina() {
		return kolicina;
	}
	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}
	@Override
	public String toString() {
		return "\nrestoran=" + restoran +
				"\nnaziv=" + naziv +
				"\ncena=" + cena + 
				"\nopis=" + opis + 
				"\nkolicina=" + kolicina;
	}
	
}
