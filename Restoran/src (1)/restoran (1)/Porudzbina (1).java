package restoran;

public class Porudzbina {
	private Artikal jelo;
	private String datum;
	private String  dostavljac;
	
	public Porudzbina() {
		this.jelo = jelo;
		this.datum = "";
		this.dostavljac = "";
	}
	
	public Porudzbina(Artikal jelo,String datum,String dostavljac) {
		this.jelo = jelo;
		this.datum = datum;
		this.dostavljac = dostavljac;		
	}
	public Porudzbina (Porudzbina original){
		this.jelo = original.jelo;
		this.datum = original.datum;
		this.dostavljac = original.dostavljac;
	}

	public Artikal getJelo() {
		return jelo;
	}

	public void setJelo(Artikal jelo) {
		this.jelo = jelo;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getDostavljac() {
		return dostavljac;
	}

	public void setDostavljac(String dostavljac) {
		this.dostavljac = dostavljac;
	}

	@Override
	public String toString() {
		return "Porudzbina [jelo=" + jelo + ", datum=" + datum + ", dostavljac=" + dostavljac + "]";
	}
	
}
