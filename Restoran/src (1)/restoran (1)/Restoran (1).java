package restoran;

public class Restoran {
	private String naziv;
	private String kategorija;
	

	public Restoran(){
		this.naziv = "";
		this.kategorija = "";
	
	}
	public Restoran(String naziv,String kategorija){
		this.naziv = naziv;
		this.kategorija = kategorija;
		
	}
	public Restoran(Restoran original) {
		this.naziv = original.naziv;
		this.kategorija = original.kategorija;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getKategorija() {
		return kategorija;
	}
	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}
	@Override
	public String toString() {
		return "Restoran [naziv=" + naziv + ", kategorija=" + kategorija + "]";
	}
}
